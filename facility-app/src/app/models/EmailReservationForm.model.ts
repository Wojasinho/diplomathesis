export class EmailReservationForm {
  nameLastname: string;
  eMail: string;
  specialisation: string;
  date: string;
  doctor: string;
  medicalCenter: string;
}
