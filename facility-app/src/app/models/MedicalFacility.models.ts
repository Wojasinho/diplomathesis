export class MedicalFacility {
  startAddress: any;
  medicalFacilityAddress: string;
  distanceBetweenMedicalFacilityAndUserPlace: number;
  estimatedTime: string;
}
