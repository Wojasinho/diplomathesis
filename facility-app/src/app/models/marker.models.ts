export class Marker {
  id;
  lat;
  lng;
  label;
  draggable;
}
