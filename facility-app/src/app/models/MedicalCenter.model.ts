export class MedicalCenter {
  id;
  name;
  latitude;
  longitude;
  phoneNumber;
  dragable;
  idAddressFk;
}
