export class Appointment {
  id: number;
  specialisation: string;
  date: Date;
  medicalCenter: string;
  doctor: string;
  distanse: number;

  constructor() {
  }
}
