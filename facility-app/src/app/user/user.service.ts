import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {User} from 'src/app/models/user.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {

  private customerUrl = 'api/users';  // URL to web api
  private currentUser;
  private username;
  public isUserLogin: boolean;

  constructor(private http: HttpClient) {
    this.currentUser = false;
  }

  setCurrentUser() {
    this.currentUser = true;
    this.username = 'admin';
  }

  getCurrentUser() {
    this.isUserLogin = true;
    return this.currentUser;
  }

  /** GET cars from the server */
  getUsers (): Observable<User[]> {
    return this.http.get<User[]>(this.customerUrl).pipe(
      catchError(this.handleError('getUsers', []))
    );
  }

  getUser(id: number): Observable<User> {
    const url = `${this.customerUrl}/${id}`;
    return this.http.get<User>(url).pipe(
      catchError(this.handleError<User>(`getUser id=${id}`))
    );
  }

  updateUser (user: User): Observable<any> {
    return this.http.put(this.customerUrl, user, httpOptions).pipe(
      catchError(this.handleError<any>('updateUser'))
    );
  }

  addUser (user: User): Observable<User> {
    return this.http.post<User>(this.customerUrl, user, httpOptions).pipe(
      catchError(this.handleError<User>('addUser'))
    );
  }

  deleteUser (user: User | number): Observable<User> {
    const id = typeof user === 'number' ? user : user.id;
    const url = `${this.customerUrl}/${id}`;

    return this.http.delete<User>(url, httpOptions).pipe(
      catchError(this.handleError<User>('deleteUser'))
    );
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      return of(result as T);
    };
  }
}
