import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {User} from 'src/app/models/user.model';
import {UserService} from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input() reservations;
  rentForUser = [];
  reservation: any[];
  users: User[];
  user: Object = {};
  key: any;
  reverse = false;
  p = 1;
  name = '';
  subscription: Subscription;

  constructor(private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.userService.getUsers()
      .subscribe(users => this.users = users);
  }

  add(username: string, password: string, name: string, surname: string, email: string, dateOfBirthday: Date): void {
    this.userService.addUser({username, password, name, surname, email, dateOfBirthday} as User).subscribe(user => {
      this.users.push(user);
    });
  }

  delete(user: User): void {
    this.users = this.users.filter(h => h !== user);
    this.userService.deleteUser(user).subscribe();
  }

  sort(key): void {
    this.key = key; //change the direction
    this.reverse = !this.reverse;
  }


}
