import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from './user.service';
import {UserComponent} from './user.component';
import {User} from '../models/user.model';

@Component({
  templateUrl: './add-user.component.html'
})
export class AddUserComponent {

  user: User = new User();

  constructor(private router: Router, private userService: UserService) {

  }

  // createUser(): void {
  //   this.userService.createUser(this.user)
  //       .subscribe( data => {
  //         alert('User created successfully.');
  //         this.exit();
  //         // this.router.navigate(['AddUserComponent']);
  //       });
  //
  // }
  exit(): void {
    location.reload();

  }

}
