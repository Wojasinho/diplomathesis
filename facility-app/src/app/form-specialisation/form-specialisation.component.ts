import { Component, OnInit } from '@angular/core';
import {Specialisation} from '../specialisation';
import {SPECIALISATIONS} from '../mock-specialisation';

@Component({
  selector: 'app-form-specialisation',
  templateUrl: './form-specialisation.component.html',
  styleUrls: ['./form-specialisation.component.css']
})

export class FormSpecialisationComponent implements OnInit {

  public listOneWithSpecialisations: Specialisation[];
  public listTwoWithSpecialisations: Specialisation[];
  public listThreeWithSpecialsations: Specialisation[];
  specialisations = SPECIALISATIONS;
  selectedSpecialisation: Specialisation;

  constructor() {
  }

  ngOnInit() {
    this.dividerSpecialisationForMenu();
  }

  onSelect(specialisation: Specialisation): void {
    this.selectedSpecialisation = specialisation;
  }

  public dividerSpecialisationForMenu() {

    this.listOneWithSpecialisations = [];
    this.listTwoWithSpecialisations = [];
    this.listThreeWithSpecialsations = [];

    for (let i = 0; i <= this.specialisations.length; i = i + 3) {
      this.listOneWithSpecialisations.push(this.specialisations[i]);
    }

    for (let i = 1; i <= this.specialisations.length; i = i + 3) {
      this.listTwoWithSpecialisations.push(this.specialisations[i]);
    }

    for (let i = 3; i <= this.specialisations.length; i = i + 3) {
      this.listThreeWithSpecialsations.push(this.specialisations[i]);
    }
  }

  public getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
