import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap' ;
import {AppComponent} from './app.component';
import {UserComponent} from './user/user.component';
import {AddUserComponent} from './user/add-user.component';
import {UserService} from './user/user.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routing.module';
import {MapsComponent} from './maps/maps.component';
import {MapsService} from './maps/maps.service';
import {AgmDirectionModule} from 'agm-direction';
import {FormSpecialisationComponent} from './form-specialisation/form-specialisation.component';   // agm-direction
import {
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatToolbarModule,
  MatDatepickerModule
} from '@angular/material';
import {PersonalDataComponent} from './personal-data/personal-data.component';
import {HeaderMenuComponent} from './header-menu/header-menu.component';
import {DetailAppointmentComponent} from './detail-appointment/detail-appointment.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '../../node_modules/@angular/common';
import {ListsAppointmentComponent} from './lists-appointment/lists-appointment.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './auth.guard';
import {AuthService} from './auth.service';
import {Ng2OrderModule} from 'ng2-order-pipe';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {AgmCoreModule} from '../../node_modules/@agm/core';
import {AppointmentService} from './detail-appointment/appointment.service';
// import {Ng2SearchPipeModule} from 'ng2-search-filter';
// import {Ng2OrderModule} from 'ng2-order-pipe';
// import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    AddUserComponent,
    MapsComponent,
    FormSpecialisationComponent,
    PersonalDataComponent,
    HeaderMenuComponent,
    DetailAppointmentComponent,
    ListsAppointmentComponent,
    LoginComponent,
    HomeComponent,
    FooterComponent,

  ],
  imports: [

    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDkXo4HIPEdjsPbJYZOmlrreIUUojNs9t0',
      libraries: ['geometry']
    }),
    NgbModule.forRoot(),
    AgmDirectionModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    BrowserAnimationsModule,
    CommonModule,
    BrowserModule, ReactiveFormsModule, Ng2OrderModule, NgxPaginationModule, Ng2SearchPipeModule,
    // Ng2SearchPipeModule, //including into imports
    // Ng2OrderModule, // importing the sorting package here
    // NgxPaginationModule
  ],
  providers: [UserService, MapsService, AuthGuard, LoginComponent, AuthService, AppointmentService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
