import {Component, Input, OnInit, ViewChild, ElementRef} from '@angular/core';
import {AppointmentService} from '../detail-appointment/appointment.service';
import {Appointment} from '../models/Appointment.model';
import {MedicalFacility} from '../models/MedicalFacility.models';
import {Observable} from 'rxjs';
import {EmailReservationForm} from '../models/EmailReservationForm.model';

@Component({
  selector: 'app-lists-appointment',
  templateUrl: './lists-appointment.component.html',
  styleUrls: ['./lists-appointment.component.css']
})

export class ListsAppointmentComponent implements OnInit{

  @ViewChild('fileInput') fileInput: ElementRef;
  @Input() listsModelsOfAppointments: Appointment[];
  @Input() listOfMedicalFacility: MedicalFacility[];
  @Input() observables: Observable<any>[];

  public listsJsonsOfAppointments: any[];
  public nameLastname: string;
  public eMail: string;

  key: any = 'distanse';
  reverse = true;
  currentAppointmentId;
  newReservation: any;

  getAllOrthopedistAppointment() {
    this.listsJsonsOfAppointments = [];
    this.appointmentService.findOrthopedistAppointment('Ortopedia').subscribe(dataJSON => {
      this.listsJsonsOfAppointments.push(dataJSON);
    });
  }

  constructor(private appointmentService: AppointmentService) {
  }

  ngOnInit() {
    this.sort('distanse')  }

  sort(key): void {
    this.key = key; //change the direction
    this.reverse = !this.reverse;
  }

  getId(appointment: Appointment) {
    const id = this.listsModelsOfAppointments.indexOf(appointment);
    this.currentAppointmentId = id;
  }

  changer() {
    this.listsModelsOfAppointments[0].distanse = this.listOfMedicalFacility[0].distanceBetweenMedicalFacilityAndUserPlace;
    this.listsModelsOfAppointments[1].distanse = this.listOfMedicalFacility[1].distanceBetweenMedicalFacilityAndUserPlace;
    this.listsModelsOfAppointments[2].distanse = this.listOfMedicalFacility[2].distanceBetweenMedicalFacilityAndUserPlace;
  }

  deleteRow(d) {
    const index = this.listsModelsOfAppointments.indexOf(d);
    this.listsModelsOfAppointments.splice(index, 1);
  }

  createReservation(id: any, nameLastname: string, eMail: string) {
    let newReservation: EmailReservationForm;
    newReservation = new EmailReservationForm();
    newReservation.nameLastname = nameLastname;
    newReservation.eMail = eMail;
    newReservation.specialisation = this.listsModelsOfAppointments[this.currentAppointmentId].specialisation;
    newReservation.date = this.listsModelsOfAppointments[this.currentAppointmentId].date.toDateString();
    newReservation.doctor = this.listsModelsOfAppointments[this.currentAppointmentId].doctor;
    newReservation.medicalCenter = this.listsModelsOfAppointments[this.currentAppointmentId].medicalCenter;
    this.appointmentService.sendEmailConfirmation(newReservation).subscribe();
    this.appointmentService.updateAppointment(id.toString()).subscribe();
    this.deleteRowAfterReservation(this.currentAppointmentId);
  }

  deleteRowAfterReservation(index) {
    this.listsModelsOfAppointments.splice(index, 1);
  }
}
