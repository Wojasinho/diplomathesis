export class Specialisation {
  id: number;
  name: string;
  choosenSpec: string;
  backgroundNrTile: number
}
