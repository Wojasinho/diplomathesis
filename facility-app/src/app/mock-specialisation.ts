import {Specialisation} from './specialisation';

export const SPECIALISATIONS: Specialisation[] = [
  {id: 11, name: 'Ortopedia', choosenSpec: 'Ortopedę',backgroundNrTile:1},
  {id: 12, name: 'Stomatologia', choosenSpec: 'Dentystę',backgroundNrTile:2},
  {id: 13, name: 'Chirurgia', choosenSpec: 'Chirurga',backgroundNrTile:3},
  {id: 14, name: 'Okulistyka', choosenSpec: 'Okulistę',backgroundNrTile:2},
  {id: 15, name: 'Radiologia', choosenSpec: 'Radiologa',backgroundNrTile:1},
  {id: 15, name: 'Pediatria', choosenSpec: 'Pediatrę',backgroundNrTile:3},
];
