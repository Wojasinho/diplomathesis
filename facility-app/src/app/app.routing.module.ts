import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserComponent} from './user/user.component';
import {AddUserComponent} from './user/add-user.component';
import {MapsComponent} from 'src/app/maps/maps.component';
import {FormSpecialisationComponent} from './form-specialisation/form-specialisation.component';
import {PersonalDataComponent} from './personal-data/personal-data.component';
import {DetailAppointmentComponent} from './detail-appointment/detail-appointment.component';
import {ListsAppointmentComponent} from './lists-appointment/lists-appointment.component';
import {AuthGuard} from './auth.guard';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {FooterComponent} from './footer/footer.component';
import {HeaderMenuComponent} from './header-menu/header-menu.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent },
  {path: 'home',  canActivate: [AuthGuard], component: HomeComponent},
  {path: 'footer', component: FooterComponent},
  {path: 'medicalFacilities', canActivate: [AuthGuard], component: MapsComponent},
  {path: 'formSpecialisation', canActivate: [AuthGuard], component: FormSpecialisationComponent},
  {path: 'profile',canActivate: [AuthGuard], component: PersonalDataComponent},
  {path: 'detailAppointment', canActivate: [AuthGuard], component: DetailAppointmentComponent},
  {path: 'header',  canActivate: [AuthGuard], component: HeaderMenuComponent},
];

@NgModule({
  exports: [RouterModule],
  providers: [AuthGuard],
  imports: [RouterModule.forRoot(routes)],
  declarations: []
})
export class AppRoutingModule {
}
