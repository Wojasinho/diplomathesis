import {AfterViewChecked, Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Marker} from '../models/marker.models';
import {MapsService} from './maps.service';
import {MedicalFacility} from '../models/MedicalFacility.models';
import {MedicalCenter} from '../models/MedicalCenter.model';
import {Specialisation} from '../specialisation';
import {AgmDirectionModule} from 'agm-direction';
import {Appointment} from '../models/Appointment.model';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})

export class MapsComponent implements OnInit {
  @Input() listMedicalCenter: MedicalCenter[];
  @Input() listsModelsOfAppointments: Appointment[];
  @Input() new: any;
  @Input() listOfMedicalFacility: MedicalFacility[];
  @Input() insertedAddress: any;
  @Input() show: any;

  public origin: any;
  public destination: any;
  public listsJsonsOfDistances: any[];
  public listOfDistances: number[];
  public nearestFacility: any;
  public dataFromMaps: string;
  public iconForUserPositionUrl = 'http://maps.google.com/mapfiles/ms/micons/blue.png';
  public travelMode: string = 'WALKING';
  public markers: Marker[];
  public medicalCenters: MedicalCenter[];
  public isValid: boolean = false;
  public latBasedOnInsertedAddress: Number;
  public lngBasedOnInsertedAddress: Number;
  zoom: number = 13;

  constructor(private router: Router, private mapsService: MapsService) {
  }

  ngOnInit() {
    this.getCoordinates();
  }

  public getCoordinates() {
    this.mapsService.getCoordinatesBasedOnAddress(this.insertedAddress).subscribe(dataJSON => {
      if (dataJSON.status == 'OK') {
        this.latBasedOnInsertedAddress = dataJSON.results[0].geometry.location.lat;
        this.lngBasedOnInsertedAddress = dataJSON.results[0].geometry.location.lng;
        console.log('Nothing');
      } else {
        console.log('Cannot find Your destination');
        return false;
      }
    });
  };

  geta() {
    return this.isValid = true;
  }

  public renderOptions = {
    suppressMarkers: true,
  };

  public removeDirection() {
    this.show = false;
  }

  public showDirection() {
    this.show = true;
  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }

  markerDragEnd(m: Marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  getDirection(origin, destination) {
    this.origin = origin;
    this.destination = destination;
  }

  public findNearestMedicalFacility() {

    var nearest = this.listOfMedicalFacility[0];
    for (var mFacility of this.listOfMedicalFacility) {
      if (nearest.distanceBetweenMedicalFacilityAndUserPlace > mFacility.distanceBetweenMedicalFacilityAndUserPlace) {
        nearest = mFacility;
      }
    }
    this.nearestFacility = nearest.distanceBetweenMedicalFacilityAndUserPlace;
    this.showDirection();
    this.getDirection(nearest.startAddress.toString(), nearest.medicalFacilityAddress.toString());
  }
}
