import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Marker} from '../models/marker.models';
import {Observable} from '../../../node_modules/rxjs';
import {MedicalCenter} from '../models/MedicalCenter.model';
import {add} from 'winston';

const httpOptions = {
  headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
  )
};

@Injectable()
export class MapsService {
  constructor(private http: HttpClient) {
  }

  private mapUrl = '/api/map';
  private apiKey: 'AIzaSyDkXo4HIPEdjsPbJYZOmlrreIUUojNs9t0';

  public getAllMedicalCenter() {
    return this.http.get<MedicalCenter[]>(this.mapUrl);
  }

  public calculateDistance(ori: string, desc: string): Observable<any> {
    return this.http.get<any>(`https://maps.googleapis.com/maps/api/distancematrix/json?units=metrics&origins=${ori}&destinations=${desc}&key=AIzaSyDkXo4HIPEdjsPbJYZOmlrreIUUojNs9t0`, httpOptions);
  }

  public getCoordinatesBasedOnAddress(address: string): Observable<any> {
    return this.http.get<any>(
      `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=AIzaSyDkXo4HIPEdjsPbJYZOmlrreIUUojNs9t0`, httpOptions);
  }
}
