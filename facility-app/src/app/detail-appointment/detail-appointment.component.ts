import {AfterContentInit, Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {Specialisation} from '../specialisation';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AppointmentService} from './appointment.service';
import {MedicalCenter} from '../models/MedicalCenter.model';
import {Appointment} from '../models/Appointment.model';
import {MedicalFacility} from '../models/MedicalFacility.models';
import {Marker} from '../models/marker.models';
import {MapsService} from '../maps/maps.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {delay} from 'rxjs/operators';
import 'rxjs-compat/add/observable/forkJoin';
import {NgbCalendar, NgbDateParserFormatter} from '../../../node_modules/@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-detail-appointment',
  templateUrl: './detail-appointment.component.html',
  styleUrls: ['./detail-appointment.component.css']
})
export class DetailAppointmentComponent implements OnInit {
  @Input() specialisation: Specialisation;

  public observables: Observable<any>[];

  public listsJsonsOfAppointments: any[];
  public listsMedicalCenterFromAppointments: MedicalCenter[];
  public listsModelsOfAppointments: Appointment[];
  public insertedAddress: any;
  public new: any;
  public origin: any;
  public destination: any;
  public listsJsonsOfDistances: any[];
  public listOfDistances: number[];
  public nearestFacility: any;
  public dataFromMaps: string;
  public iconForUserPositionUrl = 'http://maps.google.com/mapfiles/ms/micons/blue.png';
  public listOfMedicalFacility: MedicalFacility[];
  public travelMode: string = 'WALKING';
  public markers: Marker[];
  public medicalCenters: MedicalCenter[];
  public isValid: boolean = false;
  zoom: number = 13;
  public lat: Number = 51.1129493;
  public lng: Number = 17.0004063;
  public model: any;
  public dateFromCalendar: any;
  public show: boolean = false;


  constructor(private calendar: NgbCalendar, private parserFormatter: NgbDateParserFormatter, private appointmentService: AppointmentService, private mapsService: MapsService) {
  }

  getAllAppointmentJsonBySpecialisation() {

    this.listsJsonsOfAppointments = [];
    this.appointmentService.findAppointmentBySpecialisation(this.specialisation).subscribe(dataJSON => {
      this.listsJsonsOfAppointments.push(dataJSON);
      this.getEachMedicalCenterFromJson();
    });
  }

  getAllAppointmentJsonBySpecialisationAndDate() {

    this.listsJsonsOfAppointments = [];
    this.listOfMedicalFacility = [];

    this.appointmentService.findAppointmentBySpecialisationAndDate(this.specialisation, this.dateFromCalendar).subscribe(dataJSON => {
      this.listsJsonsOfAppointments.push(dataJSON);
      this.getEachMedicalCenterFromJson();
    });
  }

  getEachMedicalCenterFromJson() {

    let listIdOfMedicalCenter: any;
    listIdOfMedicalCenter = [];

    this.listsMedicalCenterFromAppointments = [];
    this.listsModelsOfAppointments = [];
    this.observables = [];
    for (let i = 0; i <= this.listsJsonsOfAppointments[0].length - 1; i = i + 1) {

      if (!listIdOfMedicalCenter.includes(this.listsJsonsOfAppointments[0][i].medicalCenterEntity.id)) {
        listIdOfMedicalCenter.push(this.listsJsonsOfAppointments[0][i].medicalCenterEntity.id);
        this.listsMedicalCenterFromAppointments.push(this.listsJsonsOfAppointments[0][i].medicalCenterEntity);
        this.calculateDistance(this.listsJsonsOfAppointments[0][i].medicalCenterEntity, i);
        this.listsModelsOfAppointments.push(this.mapperJsonToAppointment(i));
      } else {
        this.calculateDistance(this.listsJsonsOfAppointments[0][i].medicalCenterEntity, i);
        this.listsModelsOfAppointments.push(this.mapperJsonToAppointment(i));
      }
    }
  }

  mapperJsonToAppointment(iterate: number): Appointment {

    let newAppointment: Appointment;
    newAppointment = new Appointment();
    newAppointment.id = this.listsJsonsOfAppointments[0][iterate].id;
    newAppointment.specialisation = this.listsJsonsOfAppointments[0][iterate].doctorEntity.listSpecialisations[0].specialisation;
    newAppointment.date = new Date(this.listsJsonsOfAppointments[0][iterate].dateAppointment);
    newAppointment.medicalCenter = this.listsJsonsOfAppointments[0][iterate].medicalCenterEntity.name + ', ' +
      this.listsJsonsOfAppointments[0][iterate].medicalCenterEntity.addressEntity.street + ', ' +
      this.listsJsonsOfAppointments[0][iterate].medicalCenterEntity.addressEntity.num + ', ' +
      this.listsJsonsOfAppointments[0][iterate].medicalCenterEntity.addressEntity.city;
    newAppointment.doctor = this.listsJsonsOfAppointments[0][iterate].doctorEntity.gradeEntity.gradeShort + ' ' + this.listsJsonsOfAppointments[0][iterate].doctorEntity.name + ' ' +
      this.listsJsonsOfAppointments[0][iterate].doctorEntity.lastname;
    return newAppointment;
  }

  findNearestDoctor(address: any) {
    this.insertedAddress = address;
    this.dateFromCalendar = this.parserFormatter.format(this.model);
    this.getAllAppointmentJsonBySpecialisationAndDate();

  }

  ngOnInit(): void {
  }

  calculateDistanceBetweenPlaces(): boolean {

    var origins = this.insertedAddress;
    this.listsJsonsOfDistances = [];

    for (let medicalCenter of this.listsMedicalCenterFromAppointments) {
      const distanceForNextMedicalFacility =
        this.mapsService.calculateDistance(origins, (medicalCenter.latitude.toString() + ', ' + medicalCenter.longitude.toString())).subscribe(dataJSON => {
            if (dataJSON.status == 'OK') {
              if (dataJSON.rows[0].elements[0].status == 'OK') {
                this.listsJsonsOfDistances.push(dataJSON);
              } else {
                console.log('Cannot find Your destination');
                return false;
              }
            } else {
              console.log('Cannot find Your destination');
              return false;
            }
          },
          err => {
            console.log('Application didn\'t calculate destinations');
            return false;
          });
    }
    this.toMedicalFacilityDistanceModels();

    return true;
  }

  calculateDistance(medicalCenterEntity, i): boolean {

    var origins = this.insertedAddress;
    let destination: string;
    this.listsJsonsOfDistances = [];
    destination = medicalCenterEntity.latitude.toString() + ', ' + medicalCenterEntity.longitude.toString();
    this.mapsService.calculateDistance(origins, (destination)).subscribe(dataJSON => {
        if (dataJSON.status == 'OK') {
          if (dataJSON.rows[0].elements[0].status == 'OK') {
            this.parseJsonDistanse(dataJSON, i);
          } else {
            console.log('Cannot find Your destination');
            return false;
          }
        } else {
          console.log('Cannot find Your destination');
          return false;
        }
      },
      err => {
        console.log('Application didn\'t calculate destinations');
        return false;
      });
    return true;
  }

  public toMedicalFacilityDistanceModels() {

    for (let jsonData of this.listsJsonsOfDistances) {

      let medicalFacilityDistance = {} as MedicalFacility;
      medicalFacilityDistance.startAddress = jsonData.origin_addresses;
      medicalFacilityDistance.medicalFacilityAddress = jsonData.destination_addresses;
      medicalFacilityDistance.distanceBetweenMedicalFacilityAndUserPlace = this.parseDistanceToNumberFormat(jsonData.rows[0].elements[0].distance.text);
      medicalFacilityDistance.estimatedTime = jsonData.rows[0].elements[0].duration.text;
      this.listOfMedicalFacility.push(medicalFacilityDistance);
    }
  }

  public parseJsonDistanse(dataJson, i) {

    let medicalFacilityDistance = {} as MedicalFacility;
    medicalFacilityDistance.startAddress = dataJson.origin_addresses;
    medicalFacilityDistance.medicalFacilityAddress = dataJson.destination_addresses;
    medicalFacilityDistance.distanceBetweenMedicalFacilityAndUserPlace = this.parseDistanceToNumberFormat(dataJson.rows[0].elements[0].distance.text);
    medicalFacilityDistance.estimatedTime = dataJson.rows[0].elements[0].duration.text;
    this.listsModelsOfAppointments[i].distanse = medicalFacilityDistance.distanceBetweenMedicalFacilityAndUserPlace;
    this.listOfMedicalFacility.push(medicalFacilityDistance);
  }

  parseDistanceToNumberFormat(distance): number {
    if (distance.charAt(distance.length - 2) == 'k' && distance.charAt(distance.length - 1) == 'm') {
      return (+distance.slice(0, distance.length - 3));
    }

    if ((distance.slice(distance.length - 2, distance.length) == ' m')) {
      return +('0.' + (+distance.slice(0, distance.length - 2)));
    } else {
      return null;
    }
  }
}
