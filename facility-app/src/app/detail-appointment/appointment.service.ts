import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import {Observable, of} from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

import {User} from '../models/user.model';
import {catchError} from 'rxjs/operators';
import {EmailReservationForm} from '../models/EmailReservationForm.model';
import {HttpClient, HttpHeaders} from '../../../node_modules/@angular/common/http';
import {Specialisation} from '../specialisation';

const httpOptions = {
  headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    }
  )
};

@Injectable()
export class AppointmentService {

  private detailAppointmentUrl = '/api/detailAppointment/';
  private emailUrl = '/api/email';

  constructor(private http: HttpClient) {
  }

  public findAppointmentBySpecialisation(specialisation: Specialisation): Observable<any> {
    return this.http.get<any>(`${this.detailAppointmentUrl}/${specialisation.name}`);
  }

  public findOrthopedistAppointment(specialisation: any): Observable<any> {
    return this.http.get<any>(`${this.detailAppointmentUrl}/${specialisation.name}`);
  }

  public findAppointmentBySpecialisationAndDate(specialisation: any, dateAppointment: any): Observable<any> {
    return this.http.get<any>(`${this.detailAppointmentUrl}/${specialisation.name}/${dateAppointment}`);
  }

  sendEmailConfirmation(rawDataOfReservation: EmailReservationForm): Observable<any> {
    return this.http.post<any>(this.emailUrl, rawDataOfReservation);
  }

  updateAppointment(id: any): Observable<any> {
    return this.http.put<void>(`${this.detailAppointmentUrl}/${id}`, id);
  }
}
