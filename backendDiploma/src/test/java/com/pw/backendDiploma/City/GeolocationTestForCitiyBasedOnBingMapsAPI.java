package com.pw.backendDiploma.City;

import com.pw.backendDiploma.Service.HaversineService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;
import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeolocationTestForCitiyBasedOnBingMapsAPI {

    @Autowired
    private HaversineService haversineService;

    @Test
    public void testFindingWarszawaBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23194444");
        double provideLongitudeFromPrng = Double.parseDouble("21.00666667");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingWroclawBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11");
        double provideLongitudeFromPrng = Double.parseDouble("17.03194444");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKrakowBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Kraków";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.06166667");
        double provideLongitudeFromPrng = Double.parseDouble("19.9375");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKoronowoBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.31");
        double provideLongitudeFromPrng = Double.parseDouble("17.94222222");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPoznanBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.40805556");
        double provideLongitudeFromPrng = Double.parseDouble("16.93333333");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZakopaneBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Zakopane";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("49.29694444");
        double provideLongitudeFromPrng = Double.parseDouble("19.95");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingOlsztynBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Olsztyn";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.77888889");
        double provideLongitudeFromPrng = Double.parseDouble("20.48");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingBydgoszczBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.12527778");
        double provideLongitudeFromPrng = Double.parseDouble("18.00138889");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGnieznoBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Gniezno";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.53333333");
        double provideLongitudeFromPrng = Double.parseDouble("17.60138889");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZaryBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Żary";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.63583333");
        double provideLongitudeFromPrng = Double.parseDouble("15.13833333");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLodzBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Łódź";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.76444444");
        double provideLongitudeFromPrng = Double.parseDouble("19.46305556");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPlockBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Płock";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.54083333");
        double provideLongitudeFromPrng = Double.parseDouble("19.68972222");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPiasecznoBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Piaseczno";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.06972222");
        double provideLongitudeFromPrng = Double.parseDouble("21.02694444");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLublinBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Lublin";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.24777778");
        double provideLongitudeFromPrng = Double.parseDouble("22.56805556");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLubinBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Lubin";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.39861111");
        double provideLongitudeFromPrng = Double.parseDouble("16.20333333");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGorzowWielkopolskiBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Gorzów Wielkopolski";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.73194444");
        double provideLongitudeFromPrng = Double.parseDouble("15.23944444");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKatowiceBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Katowice";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.25972222");
        double provideLongitudeFromPrng = Double.parseDouble("19.01333333");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSzczecinekBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Szczecinek";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.70277778");
        double provideLongitudeFromPrng = Double.parseDouble("16.70055556");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGrudziadzBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Grudziądz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.48805556");
        double provideLongitudeFromPrng = Double.parseDouble("18.75305556");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingTarnowBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Tarnów";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.01222222");
        double provideLongitudeFromPrng = Double.parseDouble("20.98527778");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingRzeszowBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Rzeszów";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.04");
        double provideLongitudeFromPrng = Double.parseDouble("22.00");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLomzaBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Łomża";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.17861111");
        double provideLongitudeFromPrng = Double.parseDouble("22.07555556");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingBialystokBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Białystok";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.1325");
        double provideLongitudeFromPrng = Double.parseDouble("23.16305556");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingCzestochowaBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Częstochowa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.80833333");
        double provideLongitudeFromPrng = Double.parseDouble("19.12722222");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingRybnikBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Rybnik";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.09472222");
        double provideLongitudeFromPrng = Double.parseDouble("18.54222222");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZaganBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Żagań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.61555556");
        double provideLongitudeFromPrng = Double.parseDouble("15.31333333");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingOstrowWielkopolskiBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Ostrów Wielkopolski";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.64944444");
        double provideLongitudeFromPrng = Double.parseDouble("17.81666667");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingMinskMazowieckiBasedOnBingMapsAlgorithm() {
        // Given :
        String city = "Mińsk Mazowiecki";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.17833333");
        double provideLongitudeFromPrng = Double.parseDouble("21.57138889");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + city + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");

        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);

        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }


}
