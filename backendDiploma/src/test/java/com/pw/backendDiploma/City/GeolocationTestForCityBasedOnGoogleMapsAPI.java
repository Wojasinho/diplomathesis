package com.pw.backendDiploma.City;

import com.pw.backendDiploma.Service.HaversineService;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.logging.Logger;

import static io.restassured.RestAssured.*;
import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GeolocationTestForCityBasedOnGoogleMapsAPI {

    @Autowired
    private HaversineService haversineService;

    @Test
    public void testFindingWarszawaBasedOnGoogleAlghoritm() {

        // Given :
        String city = "Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23194444");
        double provideLongitudeFromPrng = Double.parseDouble("21.00666667");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingWroclawBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11");
        double provideLongitudeFromPrng = Double.parseDouble("17.03194444");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKrakowBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Kraków";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.06166667");
        double provideLongitudeFromPrng = Double.parseDouble("19.9375");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKoronowoBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.31");
        double provideLongitudeFromPrng = Double.parseDouble("17.94222222");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPoznanBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.40805556");
        double provideLongitudeFromPrng = Double.parseDouble("16.93333333");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZakopaneBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Zakopane";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("49.29694444");
        double provideLongitudeFromPrng = Double.parseDouble("19.95");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingOlsztynBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Olsztyn";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.77888889");
        double provideLongitudeFromPrng = Double.parseDouble("20.48");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingBydgoszczBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.12527778");
        double provideLongitudeFromPrng = Double.parseDouble("18.00138889");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGnieznoBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Gniezno";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.53333333");
        double provideLongitudeFromPrng = Double.parseDouble("17.60138889");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZaryBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Żary";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.63583333");
        double provideLongitudeFromPrng = Double.parseDouble("15.13833333");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLodzBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Łódź";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.76444444");
        double provideLongitudeFromPrng = Double.parseDouble("19.46305556");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPlockBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Płock";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.54083333");
        double provideLongitudeFromPrng = Double.parseDouble("19.68972222");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPiasecznoBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Piaseczno";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.06972222");
        double provideLongitudeFromPrng = Double.parseDouble("21.02694444");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLublinBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Lublin";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.24777778");
        double provideLongitudeFromPrng = Double.parseDouble("22.56805556");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLubinBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Lubin";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.39861111");
        double provideLongitudeFromPrng = Double.parseDouble("16.20333333");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGorzowWielkopolskiBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Gorzów Wielkopolski";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.73194444");
        double provideLongitudeFromPrng = Double.parseDouble("15.23944444");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKatowiceBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Katowice";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.25972222");
        double provideLongitudeFromPrng = Double.parseDouble("19.01333333");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSzczecinekBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Szczecinek";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.70277778");
        double provideLongitudeFromPrng = Double.parseDouble("16.70055556");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGrudziadzBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Grudziądz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.48805556");
        double provideLongitudeFromPrng = Double.parseDouble("18.75305556");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingTarnowBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Tarnów";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.01222222");
        double provideLongitudeFromPrng = Double.parseDouble("20.98527778");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingRzeszowBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Rzeszów";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.04");
        double provideLongitudeFromPrng = Double.parseDouble("22.00");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLomzaBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Łomża";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.17861111");
        double provideLongitudeFromPrng = Double.parseDouble("22.07555556");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingBialystokBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Białystok";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.1325");
        double provideLongitudeFromPrng = Double.parseDouble("23.16305556");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingCzestochowaBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Częstochowa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.80833333");
        double provideLongitudeFromPrng = Double.parseDouble("19.12722222");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingRybnikBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Rybnik";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.09472222");
        double provideLongitudeFromPrng = Double.parseDouble("18.54222222");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZaganBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Żagań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.61555556");
        double provideLongitudeFromPrng = Double.parseDouble("15.31333333");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingOstrowWielkopolskiBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Ostrów Wielkopolski";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.64944444");
        double provideLongitudeFromPrng = Double.parseDouble("17.81666667");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingMinskMazowieckiBasedOnGoogleAlghoritm() {
        // Given :
        String city = "Mińsk Mazowiecki";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.17833333");
        double provideLongitudeFromPrng = Double.parseDouble("21.57138889");

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + city + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("results.geometry.location.lat");
        String foundLongitude = jsonPathEvaluator.getString("results.geometry.location.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GoogleMapsApi for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void oneTest() {
        RestAssured.config =
                newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        given()
                .get("https://maps.googleapis.com/maps/api/geocode/json?address=Wrocław&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0")
                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .contentType(ContentType.JSON)
                .and()
                .body("results[0].geometry.location.lat", is(new BigDecimal("51.1078852")));
    }


}
