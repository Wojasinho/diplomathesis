package com.pw.backendDiploma.City;

import com.pw.backendDiploma.Service.HaversineService;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeolocationTestForCityBasedOnGraphHopperAPI {

    @Autowired
    private HaversineService haversineService;

    @Test
    public void testFindingWarszawaBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23194444");
        double provideLongitudeFromPrng = Double.parseDouble("21.00666667");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingWroclawBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11");
        double provideLongitudeFromPrng = Double.parseDouble("17.03194444");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKrakowBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Kraków";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.06166667");
        double provideLongitudeFromPrng = Double.parseDouble("19.9375");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKoronowoBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.31");
        double provideLongitudeFromPrng = Double.parseDouble("17.94222222");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPoznanBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.40805556");
        double provideLongitudeFromPrng = Double.parseDouble("16.93333333");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZakopaneBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Zakopane";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("49.29694444");
        double provideLongitudeFromPrng = Double.parseDouble("19.95");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingOlsztynBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Olsztyn";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.77888889");
        double provideLongitudeFromPrng = Double.parseDouble("20.48");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingBydgoszczBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.12527778");
        double provideLongitudeFromPrng = Double.parseDouble("18.00138889");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGnieznoBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Gniezno";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.53333333");
        double provideLongitudeFromPrng = Double.parseDouble("17.60138889");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZaryBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Żary";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.63583333");
        double provideLongitudeFromPrng = Double.parseDouble("15.13833333");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLodzBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Łódź";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.76444444");
        double provideLongitudeFromPrng = Double.parseDouble("19.46305556");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPlockBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Płock";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.54083333");
        double provideLongitudeFromPrng = Double.parseDouble("19.68972222");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPiasecznoBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Piaseczno";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.06972222");
        double provideLongitudeFromPrng = Double.parseDouble("21.02694444");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLublinBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Lublin";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.24777778");
        double provideLongitudeFromPrng = Double.parseDouble("22.56805556");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLubinBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Lubin";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.39861111");
        double provideLongitudeFromPrng = Double.parseDouble("16.20333333");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGorzowWielkopolskiBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Gorzów Wielkopolski";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.73194444");
        double provideLongitudeFromPrng = Double.parseDouble("15.23944444");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKatowiceBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Katowice";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.25972222");
        double provideLongitudeFromPrng = Double.parseDouble("19.01333333");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSzczecinekBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Szczecinek";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.70277778");
        double provideLongitudeFromPrng = Double.parseDouble("16.70055556");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGrudziadzBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Grudziądz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.48805556");
        double provideLongitudeFromPrng = Double.parseDouble("18.75305556");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingTarnowBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Tarnów";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.01222222");
        double provideLongitudeFromPrng = Double.parseDouble("20.98527778");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingRzeszowBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Rzeszów";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.04");
        double provideLongitudeFromPrng = Double.parseDouble("22.00");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLomzaBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Łomża";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.17861111");
        double provideLongitudeFromPrng = Double.parseDouble("22.07555556");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingBialystokBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Białystok";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.1325");
        double provideLongitudeFromPrng = Double.parseDouble("23.16305556");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingCzestochowaBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Częstochowa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.80833333");
        double provideLongitudeFromPrng = Double.parseDouble("19.12722222");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingRybnikBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Rybnik";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("50.09472222");
        double provideLongitudeFromPrng = Double.parseDouble("18.54222222");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZaganBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Żagań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.61555556");
        double provideLongitudeFromPrng = Double.parseDouble("15.31333333");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingOstrowWielkopolskiBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Ostrów Wielkopolski";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.64944444");
        double provideLongitudeFromPrng = Double.parseDouble("17.81666667");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingMinskMazowieckiBasedOnGraphHopperAlgorithm() {
        // Given :
        String city = "Mińsk Mazowiecki";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.17833333");
        double provideLongitudeFromPrng = Double.parseDouble("21.57138889");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + city + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + city + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

}
