package com.pw.backendDiploma.Address;

import com.pw.backendDiploma.Service.HaversineService;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeolocationTestForAddressBasedOnCoordinatesInGraphHopperAPI {


    @Autowired
    private HaversineService haversineService;

    @Test
    public void testFindingFabryczna8aNowogrodBobrzanskiBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Fabryczna 8A, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.8115");
        double provideLongitudeFromPrng = Double.parseDouble("15.2412");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;


        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingZarska8NowogrodBobrzanskiBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Żarska 8, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.79616");
        double provideLongitudeFromPrng = Double.parseDouble("15.23422");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingCicha3aNowogrodBobrzanskiBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Cicha 3A, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.80262");
        double provideLongitudeFromPrng = Double.parseDouble("15.23568");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingSzybka7WroclawBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Szybka 7, Wroclaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10244");
        double provideLongitudeFromPrng = Double.parseDouble("17.05408");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingLegnicka48HWroclawBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Legnicka 48, Wroclaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11841111");
        double provideLongitudeFromPrng = Double.parseDouble("16.99694722");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingRobotnicza3WroclawBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Robotnicza 3, Wroclaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10831");
        double provideLongitudeFromPrng = Double.parseDouble("17.01551");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingSlubicka7WroclawBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Słubicka 7, Wroclaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11765");
        double provideLongitudeFromPrng = Double.parseDouble("17.00141");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingGwiazdzista12WroclawBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Gwiaździsta 12, Wroclaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.09774");
        double provideLongitudeFromPrng = Double.parseDouble("17.02328");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingStrzegomska52WroclawBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Strzegomska 52, Wroclaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11343611");
        double provideLongitudeFromPrng = Double.parseDouble("16.98386667");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingRynek50WroclawBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Rynek 50, Wroclaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11081389");
        double provideLongitudeFromPrng = Double.parseDouble("17.03238333");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingPiotraNorblina36WroclawBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Piotra Norblina 36, Wroclaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10283611");
        double provideLongitudeFromPrng = Double.parseDouble("17.10906389");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingNowySwiat40WarszawaBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "New World 40, Warsaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23428333");
        double provideLongitudeFromPrng = Double.parseDouble("21.01911667");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingSolec61WarszawaBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Solec 61, Warsaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23132778");
        double provideLongitudeFromPrng = Double.parseDouble("21.03993056");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingMarszalkowska37WarszawaBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Marshal Street 37, Warsaw";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.21881944");
        double provideLongitudeFromPrng = Double.parseDouble("21.01786667");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingJanaIIISobieskiego19TopolinBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Jana III Sobieskiego 19, Topolin";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.24698611");
        double provideLongitudeFromPrng = Double.parseDouble("20.745475");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingStarowiejska174SiedlceBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Starowiejska 174, Siedlce";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.16355");
        double provideLongitudeFromPrng = Double.parseDouble("22.30476389");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingKasztanowa37SiedlceBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Kasztanowa 37, Siedlce";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.16245833");
        double provideLongitudeFromPrng = Double.parseDouble("22.32030833");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingAlejeWolnosci5GKoronowoBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Aleje Wolności 5g, Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.311325");
        double provideLongitudeFromPrng = Double.parseDouble("17.95306111");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingFarna10KoronowoBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Farna 10, Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.31514167");
        double provideLongitudeFromPrng = Double.parseDouble("17.93890833");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingLitewska17PoznanBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Litewska 17, Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.42031389");
        double provideLongitudeFromPrng = Double.parseDouble("16.90270278");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;

        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingKolorowa2PoznanBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Kolorowa 2, Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.40513333");
        double provideLongitudeFromPrng = Double.parseDouble("16.86494444");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingArmiiPoznan57LubonBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Armii Poznań 57, Luboń";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.33091944");
        double provideLongitudeFromPrng = Double.parseDouble("16.88928889");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingMalinowa15LubonBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Malinowa 15, Luboń";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.33471389");
        double provideLongitudeFromPrng = Double.parseDouble("16.879775");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingSpokojna9BydgoszczBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Spokojna 9, Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.11507222");
        double provideLongitudeFromPrng = Double.parseDouble("18.04534");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingSwarzewska10BydgoszczBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Swarzewska 10, Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.12204444");
        double provideLongitudeFromPrng = Double.parseDouble("18.04553056");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingSiekierczyn54SiekierczynBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "null 54, Siekierczyn";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.12431111");
        double provideLongitudeFromPrng = Double.parseDouble("15.18921944");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingKanalowa2LubskoBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Kanałowa 2, Lubsko";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.79010833");
        double provideLongitudeFromPrng = Double.parseDouble("14.97167778");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingChopina26ZielonaGoraBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Fryderyka Chopina 26, Zielona Góra";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.94619722");
        double provideLongitudeFromPrng = Double.parseDouble("15.513425");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingLipowa17DobroszowWielkiBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Lipowa 17, Dobroszów Wielki";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.77446667");
        double provideLongitudeFromPrng = Double.parseDouble("15.25763889");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }

    @Test
    public void testFindingKarolaSzymanowskiego4ZaryBasedOnCoordinatesInGraphHopperAPI() {

        // Given :
        String address = "Karola Szymanowskiego 4, Żary";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.64169444");
        double provideLongitudeFromPrng = Double.parseDouble("15.12623611");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?reverse=true&point=" + coordinates + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundNumber = jsonPathEvaluator.getString("hits[0].housenumber");
        String foundStreet = jsonPathEvaluator.getString("hits[0].street");
        String foundCity = jsonPathEvaluator.getString("hits[0].city");
        String foundAddress = foundStreet + " " + foundNumber + ", " + foundCity;
        
        // Then :
        Assert.assertEquals(foundAddress, address);
    }


}
