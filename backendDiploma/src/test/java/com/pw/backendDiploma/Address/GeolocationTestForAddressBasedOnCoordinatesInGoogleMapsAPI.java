package com.pw.backendDiploma.Address;

import com.pw.backendDiploma.Service.HaversineService;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeolocationTestForAddressBasedOnCoordinatesInGoogleMapsAPI {

    @Autowired
    private HaversineService haversineService;

    @Test
    public void testFindingFabryczna8aNowogrodBobrzanskiBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Fabryczna 8A, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.8115");
        double provideLongitudeFromPrng = Double.parseDouble("15.2412");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingZarska8NowogrodBobrzanskiBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Żarska 8, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.79616");
        double provideLongitudeFromPrng = Double.parseDouble("15.23422");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingCicha3aNowogrodBobrzanskiBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Cicha 3A, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.80262");
        double provideLongitudeFromPrng = Double.parseDouble("15.23568");
        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingSzybka7WroclawBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Szybka 7, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10244");
        double provideLongitudeFromPrng = Double.parseDouble("17.05408");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingLegnicka48HWroclawBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Legnicka 48, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11841111");
        double provideLongitudeFromPrng = Double.parseDouble("16.99694722");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingRobotnicza3WroclawBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Robotnicza 3, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10831");
        double provideLongitudeFromPrng = Double.parseDouble("17.01551");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingSlubicka7WroclawBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Słubicka 7, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11765");
        double provideLongitudeFromPrng = Double.parseDouble("17.00141");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingGwiazdzista12WroclawBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Gwiaździsta 12, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.09774");
        double provideLongitudeFromPrng = Double.parseDouble("17.02328");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingStrzegomska52WroclawBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Strzegomska 52, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11343611");
        double provideLongitudeFromPrng = Double.parseDouble("16.98386667");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingRynek50WroclawBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Rynek 50, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11081389");
        double provideLongitudeFromPrng = Double.parseDouble("17.03238333");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingPiotraNorblina36WroclawBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Piotra Norblina 36, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10283611");
        double provideLongitudeFromPrng = Double.parseDouble("17.10906389");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingNowySwiat40WarszawaBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Nowy Świat 40, Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23428333");
        double provideLongitudeFromPrng = Double.parseDouble("21.01911667");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingSolec61WarszawaBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Solec 61, Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23132778");
        double provideLongitudeFromPrng = Double.parseDouble("21.03993056");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingMarszalkowska37WarszawaBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Marszałkowska 37, Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.21881944");
        double provideLongitudeFromPrng = Double.parseDouble("21.01786667");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingJanaIIISobieskiego19TopolinBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Króla Jana III Sobieskiego 19, Topolin";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.24698611");
        double provideLongitudeFromPrng = Double.parseDouble("20.745475");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingStarowiejska174SiedlceBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Starowiejska 174, Siedlce";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.16355");
        double provideLongitudeFromPrng = Double.parseDouble("22.30476389");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingKasztanowa37SiedlceBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Kasztanowa 37, Siedlce";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.16245833");
        double provideLongitudeFromPrng = Double.parseDouble("22.32030833");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingAlejeWolnosci5GKoronowoBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Aleje Wolnosci 5G, Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.311325");
        double provideLongitudeFromPrng = Double.parseDouble("17.95306111");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingFarna10KoronowoBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Farna 10, Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.31514167");
        double provideLongitudeFromPrng = Double.parseDouble("17.93890833");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingLitewska17PoznanBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Litewska 17, Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.42031389");
        double provideLongitudeFromPrng = Double.parseDouble("16.90270278");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingKolorowa2PoznanBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Kolorowa 2, Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.40513333");
        double provideLongitudeFromPrng = Double.parseDouble("16.86494444");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingArmiiPoznan57LubonBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Armii Poznań 57, Luboń";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.33091944");
        double provideLongitudeFromPrng = Double.parseDouble("16.88928889");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingMalinowa15LubonBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Malinowa 15, Luboń";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.33471389");
        double provideLongitudeFromPrng = Double.parseDouble("16.879775");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingSpokojna9BydgoszczBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Spokojna 9, Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.11507222");
        double provideLongitudeFromPrng = Double.parseDouble("18.04534");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingSwarzewska10BydgoszczBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Swarzewska 10, Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.12204444");
        double provideLongitudeFromPrng = Double.parseDouble("18.04553056");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingSiekierczyn54SiekierczynBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Siekierczyn 54, Siekierczyn";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.12431111");
        double provideLongitudeFromPrng = Double.parseDouble("15.18921944");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingKanalowa2LubskoBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Kanałowa 2, Lubsko";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.79010833");
        double provideLongitudeFromPrng = Double.parseDouble("14.97167778");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingChopina26ZielonaGoraBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Fryderyka Chopina 26, Zielona Góra";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.94619722");
        double provideLongitudeFromPrng = Double.parseDouble("15.513425");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingLipowa17DobroszowWielkiBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Lipowa 17, Dobroszów Wielki";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.77446667");
        double provideLongitudeFromPrng = Double.parseDouble("15.25763889");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }

    @Test
    public void testFindingKarolaSzymanowskiego4ZaryBasedOnCoordinatesInGoogleMapsAPI() {

        // Given :
        String address = "Karola Szymanowskiego 4, Żary";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.64169444");
        double provideLongitudeFromPrng = Double.parseDouble("15.12623611");

        String coordinates = Double.toString(provideLatitudeFromPrng) + "," + Double.toString(provideLongitudeFromPrng);

        // When :
        Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=" + coordinates + "&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundAddress = jsonPathEvaluator.getString("results[0].formatted_address");
        String[] splittedAddres = foundAddress.split(",");
        String foundedStreetAndNumber = splittedAddres[0];
        String foundedCity = splittedAddres[1].substring(8,splittedAddres[1].length());

        // Then :
        Assert.assertEquals(foundedStreetAndNumber + ", " + foundedCity, address);
    }


}
