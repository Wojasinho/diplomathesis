package com.pw.backendDiploma.Address;

import com.pw.backendDiploma.Service.HaversineService;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeolocationTestForAddressBasedOnAddressInGraphHopperAPI {

    @Autowired
    private HaversineService haversineService;

    @Test
    public void testFindingFabryczna8aNowogrodBobrzanskiBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Fabryczna 8A, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.8115");
        double provideLongitudeFromPrng = Double.parseDouble("15.2412");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZarska8NowogrodBobrzanskiBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Żarska 8, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.79616");
        double provideLongitudeFromPrng = Double.parseDouble("15.23422");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingCicha3aNowogrodBobrzanskiBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Cicha 3A, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.80262");
        double provideLongitudeFromPrng = Double.parseDouble("15.23568");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSzybka7WroclawBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Szybka 7, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10244");
        double provideLongitudeFromPrng = Double.parseDouble("17.05408");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLegnicka48HWroclawBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Legnicka 48, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11841111");
        double provideLongitudeFromPrng = Double.parseDouble("16.99694722");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingRobotnicza3WroclawBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Robotnicza 3, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10831");
        double provideLongitudeFromPrng = Double.parseDouble("17.01551");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSlubicka7WroclawBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Słubicka 7, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11765");
        double provideLongitudeFromPrng = Double.parseDouble("17.00141");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGwiazdzista12WroclawBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Gwiaździsta 12, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.09774");
        double provideLongitudeFromPrng = Double.parseDouble("17.02328");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=3d14d714-9d0a-4112-9fe2-db56fe61c184");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingStrzegomska52WroclawBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Strzegomska 52, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11343611");
        double provideLongitudeFromPrng = Double.parseDouble("16.98386667");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingRynek50WroclawBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Rynek 50, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11081389");
        double provideLongitudeFromPrng = Double.parseDouble("17.03238333");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPiotraNorblina36WroclawBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Piotra Norblina 36, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10283611");
        double provideLongitudeFromPrng = Double.parseDouble("17.10906389");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingNowySwiat40WarszawaBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Nowy Świat 40, Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23428333");
        double provideLongitudeFromPrng = Double.parseDouble("21.01911667");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSolec61WarszawaBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Solec 61, Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23132778");
        double provideLongitudeFromPrng = Double.parseDouble("21.03993056");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingMarszalkowska37WarszawaBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Marszałkowska 37, Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.21881944");
        double provideLongitudeFromPrng = Double.parseDouble("21.01786667");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingJanaIIISobieskiego19TopolinBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Jana III Sobieskiego 19, Topolin";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.24698611");
        double provideLongitudeFromPrng = Double.parseDouble("20.745475");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingStarowiejska174SiedlceBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Starowiejska 174, Siedlce";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.16355");
        double provideLongitudeFromPrng = Double.parseDouble("22.30476389");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKasztanowa37SiedlceBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Kasztanowa 37, Siedlce";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.16245833");
        double provideLongitudeFromPrng = Double.parseDouble("22.32030833");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingAlejeWolnosci5GKoronowoBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Aleje Wolnosci 5G, Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.311325");
        double provideLongitudeFromPrng = Double.parseDouble("17.95306111");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingFarna10KoronowoBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Farna 10, Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.31514167");
        double provideLongitudeFromPrng = Double.parseDouble("17.93890833");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLitewska17PoznanBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Litewska 17, Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.42031389");
        double provideLongitudeFromPrng = Double.parseDouble("16.90270278");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKolorowa2PoznanBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Kolorowa 2, Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.40513333");
        double provideLongitudeFromPrng = Double.parseDouble("16.86494444");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingArmiiPoznan57LubonBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Armii Poznań 57, Luboń";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.33091944");
        double provideLongitudeFromPrng = Double.parseDouble("16.88928889");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingMalinowa15LubonBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Malinowa 15, Luboń";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.33471389");
        double provideLongitudeFromPrng = Double.parseDouble("16.879775");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSpokojna9BydgoszczBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Spokojna 9, Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.11507222");
        double provideLongitudeFromPrng = Double.parseDouble("18.04534");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSwarzewska10BydgoszczBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Swarzewska 10, Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.12204444");
        double provideLongitudeFromPrng = Double.parseDouble("18.04553056");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSiekierczyn54SiekierczynBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Siekierczyn 54, Siekierczyn";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.12431111");
        double provideLongitudeFromPrng = Double.parseDouble("15.18921944");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKanalowa2LubskoBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Kanałowa 2, Lubsko";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.79010833");
        double provideLongitudeFromPrng = Double.parseDouble("14.97167778");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingChopina26ZielonaGoraBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Chopina 26, Zielona Góra";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.94619722");
        double provideLongitudeFromPrng = Double.parseDouble("15.513425");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLipowa17DobroszowWielkiBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Lipowa 17, Dobroszów Wielki";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.77446667");
        double provideLongitudeFromPrng = Double.parseDouble("15.25763889");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKarolaSzymanowskiego4ZaryBasedOnAddressInGraphHopperAPI() {

        // Given :
        String address = "Karola Szymanowskiego 4, Żary";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.64169444");
        double provideLongitudeFromPrng = Double.parseDouble("15.12623611");

        // When :
        Response response = httpRequest.get("https://graphhopper.com/api/1/geocode?q=" + address + "&locale=pl&debug=true&key=f3ea3c27-3c01-45e1-9117-749dc8925144");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String foundLatitude = jsonPathEvaluator.getString("hits[0].point.lat");
        String foundLongitude = jsonPathEvaluator.getString("hits[0].point.lng");
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);
        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from GraphHopperAPI for city " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }
}
