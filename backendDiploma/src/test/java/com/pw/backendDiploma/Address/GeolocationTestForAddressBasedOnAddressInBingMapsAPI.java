package com.pw.backendDiploma.Address;

import com.pw.backendDiploma.Service.HaversineService;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import javafx.beans.value.ObservableBooleanValue;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeolocationTestForAddressBasedOnAddressInBingMapsAPI {
    @Autowired
    private HaversineService haversineService;

    @Test
    public void testFindingFabryczna8aNowogrodBobrzanskiBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Fabryczna 8A, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.8115");
        double provideLongitudeFromPrng = Double.parseDouble("15.2412");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingZarska8NowogrodBobrzanskiBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Żarska 8, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.79616");
        double provideLongitudeFromPrng = Double.parseDouble("15.23422");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingCicha3aNowogrodBobrzanskiBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Cicha 3A, Nowogród Bobrzański";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.80262");
        double provideLongitudeFromPrng = Double.parseDouble("15.23568");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSzybka7WroclawBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Szybka 7, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10244");
        double provideLongitudeFromPrng = Double.parseDouble("17.05408");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLegnicka48HWroclawBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Legnicka 48, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11841111");
        double provideLongitudeFromPrng = Double.parseDouble("16.99694722");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingRobotnicza3WroclawBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Robotnicza 3, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10831");
        double provideLongitudeFromPrng = Double.parseDouble("17.01551");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSlubicka7WroclawBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Słubicka 7, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11765");
        double provideLongitudeFromPrng = Double.parseDouble("17.00141");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingGwiazdzista12WroclawBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Gwiaździsta 12, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.09774");
        double provideLongitudeFromPrng = Double.parseDouble("17.02328");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingStrzegomska52WroclawBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Strzegomska 52, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11343611");
        double provideLongitudeFromPrng = Double.parseDouble("16.98386667");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingRynek50WroclawBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Rynek 50, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.11081389");
        double provideLongitudeFromPrng = Double.parseDouble("17.03238333");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingPiotraNorblina36WroclawBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Piotra Norblina 36, Wrocław";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.10283611");
        double provideLongitudeFromPrng = Double.parseDouble("17.10906389");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingNowySwiat40WarszawaBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Nowy Świat 40, Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23428333");
        double provideLongitudeFromPrng = Double.parseDouble("21.01911667");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSolec61WarszawaBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Solec 61, Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.23132778");
        double provideLongitudeFromPrng = Double.parseDouble("21.03993056");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingMarszalkowska37WarszawaBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Marszałkowska 37, Warszawa";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.21881944");
        double provideLongitudeFromPrng = Double.parseDouble("21.01786667");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingJanaIIISobieskiego19TopolinBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Jana III Sobieskiego 19, Topolin";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.24698611");
        double provideLongitudeFromPrng = Double.parseDouble("20.745475");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingStarowiejska174SiedlceBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Starowiejska 174, Siedlce";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.16355");
        double provideLongitudeFromPrng = Double.parseDouble("22.30476389");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKasztanowa37SiedlceBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Kasztanowa 37, Siedlce";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.16245833");
        double provideLongitudeFromPrng = Double.parseDouble("22.32030833");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingAlejeWolnosci5GKoronowoBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Aleje Wolnosci 5G, Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.311325");
        double provideLongitudeFromPrng = Double.parseDouble("17.95306111");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingFarna10KoronowoBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Farna 10, Koronowo";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.31514167");
        double provideLongitudeFromPrng = Double.parseDouble("17.93890833");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLitewska17PoznanBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Litewska 17, Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.42031389");
        double provideLongitudeFromPrng = Double.parseDouble("16.90270278");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKolorowa2PoznanBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Kolorowa 2, Poznań";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.40513333");
        double provideLongitudeFromPrng = Double.parseDouble("16.86494444");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingArmiiPoznan57LubonBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Armii Poznań 57, Luboń";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.33091944");
        double provideLongitudeFromPrng = Double.parseDouble("16.88928889");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingMalinowa15LubonBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Malinowa 15, Luboń";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("52.33471389");
        double provideLongitudeFromPrng = Double.parseDouble("16.879775");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSpokojna9BydgoszczBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Spokojna 9, Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.11507222");
        double provideLongitudeFromPrng = Double.parseDouble("18.04534");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSwarzewska10BydgoszczBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Swarzewska 10, Bydgoszcz";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("53.12204444");
        double provideLongitudeFromPrng = Double.parseDouble("18.04553056");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingSiekierczyn54SiekierczynBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Siekierczyn 54, Siekierczyn";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.12431111");
        double provideLongitudeFromPrng = Double.parseDouble("15.18921944");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKanalowa2LubskoBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Kanałowa 2, Lubsko";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.79010833");
        double provideLongitudeFromPrng = Double.parseDouble("14.97167778");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingChopina26ZielonaGoraBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Chopina 26, Zielona Góra";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.94619722");
        double provideLongitudeFromPrng = Double.parseDouble("15.513425");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingLipowa17DobroszowWielkiBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Lipowa 17, Dobroszów Wielki";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.77446667");
        double provideLongitudeFromPrng = Double.parseDouble("15.25763889");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }

    @Test
    public void testFindingKarolaSzymanowskiego4ZaryBasedOnAddressInBingMapsAPI() {

        // Given :
        String address = "Karola Szymanowskiego 4, Żary";
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
        double provideLatitudeFromPrng = Double.parseDouble("51.64169444");
        double provideLongitudeFromPrng = Double.parseDouble("15.12623611");

        // When :
        Response response = httpRequest.get("http://dev.virtualearth.net/REST/v1/Locations/PL/" + address + "?o=json&key=AqRsFrd9I2DOBAFpYMhIs0ajfBMJvprnDsw6l9xZmzEQaR9xBAe11g_gUtaApwVU");
        JsonPath jsonPathEvaluator = response.jsonPath();
        String[] foundArrayLocation = jsonPathEvaluator.getString("resourceSets.resources[0].point.coordinates[0]").split(",");
        String foundLatitude = foundArrayLocation[0];
        String foundLongitude = foundArrayLocation[1];
        foundLatitude = haversineService.parserForLocation(foundLatitude);
        foundLongitude = haversineService.parserForLocation(foundLongitude);
        double foundLatitudeInDouble = Double.parseDouble(foundLatitude);
        double foundLongitudeInDouble = Double.parseDouble(foundLongitude);

        // Then :
        String foundLocation = foundLatitude + ", " + foundLongitude;
        double differenceInDistance = haversineService.calculateDistance(provideLatitudeFromPrng, provideLongitudeFromPrng, foundLatitudeInDouble, foundLongitudeInDouble);
        System.out.println("Difference beetween point from PRNG and from BingMapsAPI for address " + address + " is " + differenceInDistance + "[km]");
        Assert.assertEquals(provideLatitudeFromPrng + ", " + provideLongitudeFromPrng, foundLocation);
    }
}
