package com.pw.backendDiploma;

import com.pw.backendDiploma.Dao.AppointmentDao;
import com.pw.backendDiploma.Entity.AppointmentEntity;
import com.pw.backendDiploma.Entity.MarkerEntity;
import com.pw.backendDiploma.Entity.MedicalCenterEntity;
import com.pw.backendDiploma.Service.AppointmentService;
import com.pw.backendDiploma.Service.MarkerService;
import com.pw.backendDiploma.Service.MedicalCenterService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BackendDiplomaApplicationTests {

    @Autowired
    private MarkerService markerService;

    @Autowired
    private MedicalCenterService medicalCenterService;

    @Autowired
    private AppointmentDao appointmentDao;

    @Autowired
    private AppointmentService appointmentService;

    @Test
    public void shouldReturnSizeOfListsMarkers() {
        List<MarkerEntity> lists = new ArrayList<MarkerEntity>();
        lists = markerService.findAll();
        assertEquals(3, lists.size());
    }

    @Test
    public void shouldReturnSizeOfListMedicalCenters() {
        List<MedicalCenterEntity> listOfMedicalCenters = new ArrayList<MedicalCenterEntity>();
        listOfMedicalCenters = medicalCenterService.findAll();
        assertEquals(2, listOfMedicalCenters.size());
    }

//    @Test
//    public void shouldReturnListOfActuallAppointmentForSpecialisationAndDate() {
//        String rawDateAppointment = "2019-01-02";
//        Timestamp dateAppointment = Timestamp.valueOf(rawDateAppointment + " 00:00:00");
//        List<AppointmentEntity> listActualAppointment = appointmentDao.findAppointmentEntityBySpecialisationAndDate("Ortopedia", dateAppointment);
//        assertEquals(1,listActualAppointment.size());
//    }

    @Test
    public void shouldUpdateStatusAppointment(){
        String statusAppointment = "reserved";
        AppointmentEntity beforeAppointmentEntity = appointmentDao.findOne(1);
        beforeAppointmentEntity.setStatus(statusAppointment);

        appointmentService.update(beforeAppointmentEntity);

    }


}
