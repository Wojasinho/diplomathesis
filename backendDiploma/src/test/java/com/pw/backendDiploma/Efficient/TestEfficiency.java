package com.pw.backendDiploma.Efficient;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Test;

import java.sql.Timestamp;

import java.util.ArrayList;

import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;

public class TestEfficiency {

    @Test
    public void shouldMeasureTimeFor100LocationFromGoogleMapsAPI() {
        RestAssured.config = newConfig().jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        RequestSpecification httpRequest = RestAssured.given();
//        Instant beforeMeasure = Instant.now();
        ArrayList responses = new ArrayList<>();
        ArrayList timeResponses = new ArrayList<>();
        long start = System.currentTimeMillis();

        for (int i = 0; i < 10; i++) {
            long start1 = System.currentTimeMillis();
            Response response = httpRequest.get("https://maps.googleapis.com/maps/api/geocode/json?address=Warszawa&key=AIzaSyDhfucYvuJ7GkePuGr5cLOCvjs-INtzph0");
            long finish1 = System.currentTimeMillis()-start1;
            responses.add(response);
            timeResponses.add(finish1);
        }

        long finish = System.currentTimeMillis()-start;
        System.out.println("Print");
//        JsonPath jsonPathEvaluator = response.jsonPath();

    }
}
