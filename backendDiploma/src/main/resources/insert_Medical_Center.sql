
-- INSERT MEDICAL_CENTER
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (1, "Centrum Medicover - Strzegomska", 51.1129493, 17.0004063, "00000000", 0, 31);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (2, "Scanmed Multimedis", 51.114541, 17.073465, "00000000", 0, 32);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (3, "Dentin sp.j. NZOZ. Stomatologia", 51.102101, 17.053640, "00000000", 0, 33);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (4, "DENTAL SALON", 51.122660, 16.972160, "00000000", 0, 12);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (5, "EMC - EuroMediCare Szpital Specjalistyczny z Przychodnią", 51.141830, 16.955920, "00000000", 0, 13);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (6, "EMC - Przychodnia Formica", 51.159334, 17.131031, "00000000", 0, 14);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (7, "EMC - Przychodnia Formica", 51.137697, 16.980650, "00000000", 0, 15);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (8, "EMC - Przychodnia przy Łowieckiej", 51.124818, 17.027041, "00000000", 0, 16);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (9, "Han-Med", 51.064569, 16.978322, "00000000", 0, 17);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (10, "Multi-Med",51.116105 ,17.060152 , "00000000", 0, 18);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (11, "Centrum Medyczne Scanmed Multimedis", 51.107500 , 17.043856 , "00000000", 0, 19);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (12, "Polmed ", 51.096326 ,16.988393 , "00000000", 0, 20);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (13, "Centrum Medicover - Globis", 51.098967, 17.025920, "00000000", 0, 21);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (14, "Affidea", 51.101555, 17.005013, "00000000", 0, 22);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (15, "Affidea", 51.099808, 17.056208, "00000000", 0, 23);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (16, "NZOZ KOPEXMED", 51.102272, 17.023250, "00000000", 0, 24);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (17, "Centrum Medyczne Enel-Med. Arkady Wrocławskie", 51.099311, 17.028825, "00000000", 0, 25);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (18, "Profesorskie CM", 51.102157, 17.053753, "00000000", 0, 26);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (19, "Prestige Dent Stomatologia Sp. z o.o.", 51.108514, 17.034585, "00000000", 0, 27);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (20, "LUX MED Diagnostyka", 51.113631, 17.003128, "00000000", 0, 28);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (21, "LUX MED Diagnostyka", 51.106841, 17.038274, "00000000", 0, 29);
INSERT INTO MEDICAL_CENTER (ID_MEDICAL_CENTER, NAME, LATITUDE, LONGITUDE, PHONE_NUMBER, DRAGGABLE, ID_ADDRESS_FK) VALUES (22, "Wrocławskie Centrum Rehabilitacji i Medycyny Sportowej", 51.118568, 17.081896, "00000000", 0, 30);