
-- INSERT ADDRESS
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (1, "Szybka", "7", "50-421", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (2, "Gwiazdzista", "12", "53-413", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (3, "Polna", "3", "52-120", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (4, "Strzegomska", "52", "53-611", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (5, "Bystrzycka", "45", "54-215", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (6, "Legnicka", "57", "53-656", "Wroclaw");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (7, "Tadeusza Kościuszki ", "21", "50-026", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (8, "Komandorska", "147", "53-344", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (9, "Bezpieczna", "19", "52-443", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (10, "Cypriana Godebskiego", "2", "51-691", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (11, "Maksymiliana Jackowskiego", "57", "51-665", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (12, "Horbaczewskiego", "54A", "54-238", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (13, "Pilczycka", "144-148", "54-144", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (14, "Królewska", "30", "51-200", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (15, "Celtycka", "9/1", "54-153", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (16, "Łowiecka", "24", "50-224", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (17, "Wałbrzyska", "26", "52-314", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (18, "Sienkiewicza", "110", "50-361", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (19, "Zygmunta Krasińskiego", "9", "50-417", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (20, "Grabiszyńska", "208", "53-235", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (21, "Powstańców Sląskich", "7A", "53-332", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (22, "Grabiszyńska", "105", "53-439", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (23, "Traugutta", "116", "50-437", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (24, "Piłsudskiego", "23", "50-044", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (25, "Powstańców Slaskich", "2", "53-110", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (26, "Szybka", "5", "50-421", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (27, "Oławska", "9", "50-001", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (28, "Legnicka", "40", "53-674", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (29, "Piotra Skarga", "3", "50-082", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (30, "Chopina", "5/7", "50-001", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (31, "Strzegomska", "36", "53-611", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (32, "Parkowa", "45", "51-616", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (33, "Szybka", "3", "50-421", "Wrocław");

INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (34, "Szybka", "7", "50-421", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (35, "Zielińskiego", "10", "53-531", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (36, "Ziemowita", "1", "53-678", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (37, "Legnicka", "17", "53-671", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (38, "Cypriana Godebskiego", "2", "51-691", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (39, "Piotra Norblina", "36", "51-664", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (40, "Lotnicza", "7", "54-001", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (41, "Dzielna", "9", "54-152", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (42, "Powstańców Śląskich", "95", "53-332", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (43, "Rynek", "50", "50-996", "Wrocław");
INSERT INTO ADDRESS (ID_ADDRESS, STREET, NUM, POST_CODE, CITY) VALUES (44, "Plac Bema", "3", "50-265", "Wrocław");

