-- INSERT GRADE
INSERT INTO GRADE (ID_GRADE, GRADE_SHORT, GRADE_LONG) VALUES (1, "lek. med.", "lekarz medycyny");
INSERT INTO GRADE (ID_GRADE, GRADE_SHORT, GRADE_LONG) VALUES (2, "lek. dent." , "lekarz dentysta");
INSERT INTO GRADE (ID_GRADE, GRADE_SHORT, GRADE_LONG) VALUES (3, "dr n. med." , "doktor nauk medycznych");
INSERT INTO GRADE (ID_GRADE, GRADE_SHORT, GRADE_LONG) VALUES (4, "dr hab n. med." , "doktor habilitowany nauk medycznych");
INSERT INTO GRADE (ID_GRADE, GRADE_SHORT, GRADE_LONG) VALUES (5, "prof. dr hab" , "profesor doktor habilitowany");
INSERT INTO GRADE (ID_GRADE, GRADE_SHORT, GRADE_LONG) VALUES (6, "tech." , "technik");
INSERT INTO GRADE (ID_GRADE, GRADE_SHORT, GRADE_LONG) VALUES (7, "mgr" , "magister");
