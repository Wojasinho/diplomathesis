package com.pw.backendDiploma.Controller;

import com.pw.backendDiploma.Mail.EmailCfg;
import com.pw.backendDiploma.Mail.FormAppointmentReservation;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.MimeMessage;
import javax.xml.bind.ValidationException;
import javax.xml.ws.BindingType;
import java.io.IOException;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api/email")
public class EMailController {

    private EmailCfg emailCfg;

    public EMailController(EmailCfg emailCfg) {
        this.emailCfg = emailCfg;
    }

    @PostMapping()
    public void sendFormAppointmentReservation(@RequestBody FormAppointmentReservation receivedData) {


        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(emailCfg.getHost());
        mailSender.setPort(emailCfg.getPort());
        mailSender.setUsername(emailCfg.getUsername());
        mailSender.setPassword(emailCfg.getPassword());

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("znajdzlekarza@najblizej.pl");
        mailMessage.setTo(receivedData.geteMail());
        mailMessage.setSubject("Rezerwacja lekarza : " + receivedData.getSpecialisation());
        mailMessage.setText("Szanowny Panie : " + receivedData.getNameLastname() + "." + "Dokonano rezerwacji : " +
                "Wizyta u lekarza :" + receivedData.getSpecialisation() + " : " + receivedData.getDoctor() + ". " +
                "Wizyta odbedzie sie w " + receivedData.getMedicalCenter() + ", " + receivedData.getDate());
        mailSender.send(mailMessage);
    }
}
