package com.pw.backendDiploma.Controller;

import com.pw.backendDiploma.Dao.MarkerRepository;
import com.pw.backendDiploma.Service.MarkerService;
import com.pw.backendDiploma.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
public class MarkerController {

    @Autowired
    private MarkerService markerService;

    @GetMapping
    public List findAll() {
        return markerService.findAll();
    }
}