package com.pw.backendDiploma.Controller;

import com.pw.backendDiploma.Entity.AppointmentEntity;
import com.pw.backendDiploma.Service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/api/"})
public class AppointmentController {

    @Autowired
    private AppointmentService appointmentService;

    @RequestMapping(value = "/detailAppointment/{specialisation}", method = GET)
    public List<AppointmentEntity> findAppointmentEntityBySpecialisation(
            @PathVariable String specialisation) {
        return appointmentService.findAppointmentEntityBySpecialisation(specialisation);
    }

    @GetMapping(path = {"/detailAppointment/{specialisation}/{rawDateAppointment}"})
    public List<AppointmentEntity> findAppointmentEntityBySpecialisationAndDate(@PathVariable("specialisation" ) String specialisation,
                                                                                @PathVariable("rawDateAppointment" ) String rawDate) {
        Timestamp dateApp = Timestamp.valueOf(rawDate + " 00:00:00" );
        return appointmentService.findAppointmentEntityBySpecialisationAndDate(specialisation, dateApp);
    }

    @PutMapping(path = {"/detailAppointment/{id}"})
    public void updateStatusAppointment(@PathVariable String id) {
        int castedId = Integer.valueOf(id);
        appointmentService.updateStatusAppointment(castedId);   }
    }
