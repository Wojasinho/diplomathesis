package com.pw.backendDiploma.Controller;

import com.pw.backendDiploma.Service.MedicalCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/api/map"})
public class MedicalCenterController {

    @Autowired
    private MedicalCenterService medicalCenterService;


    @GetMapping
    public List findAll() {
        return medicalCenterService.findAll();
    }

}
