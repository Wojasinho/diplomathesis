package com.pw.backendDiploma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendDiplomaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendDiplomaApplication.class, args);
	}
}
