package com.pw.backendDiploma.Service;

import com.pw.backendDiploma.Entity.MarkerEntity;

import java.util.List;

public interface MarkerService {
    List<MarkerEntity> findAll();
}