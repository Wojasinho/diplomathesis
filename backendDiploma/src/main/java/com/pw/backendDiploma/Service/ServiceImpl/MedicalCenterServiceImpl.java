package com.pw.backendDiploma.Service.ServiceImpl;

import com.pw.backendDiploma.Dao.MarkerRepository;
import com.pw.backendDiploma.Dao.MedicalCenterDao;
import com.pw.backendDiploma.Entity.MedicalCenterEntity;
import com.pw.backendDiploma.Service.MedicalCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicalCenterServiceImpl implements MedicalCenterService {

    @Autowired
    private MedicalCenterDao medicalCenterDao;

    @Override
    public List<MedicalCenterEntity> findAll() {
        return medicalCenterDao.findAll();
    }
}