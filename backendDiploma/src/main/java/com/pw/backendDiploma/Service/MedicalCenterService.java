package com.pw.backendDiploma.Service;

import com.pw.backendDiploma.Entity.MedicalCenterEntity;

import java.util.List;

public interface MedicalCenterService {

    List<MedicalCenterEntity> findAll();
}

