package com.pw.backendDiploma.Service;

import com.pw.backendDiploma.Entity.AppointmentEntity;

import java.sql.Timestamp;
import java.util.List;

public interface AppointmentService {

    List<AppointmentEntity> findAppointmentEntityBySpecialisation(String specialisation);

    List<AppointmentEntity> findAppointmentEntityBySpecialisationAndDate(String specialisation, Timestamp dateAppointment);

    void updateStatusAppointment(int id);

    void delete(AppointmentEntity appointmentEntity);

    List<AppointmentEntity> findAll();

    AppointmentEntity findOne(int id);

    AppointmentEntity save(AppointmentEntity appointmentEntity);

    AppointmentEntity update(AppointmentEntity appointmentEntity);
}
