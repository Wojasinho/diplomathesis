package com.pw.backendDiploma.Service.ServiceImpl;

import com.pw.backendDiploma.Dao.AppointmentDao;
import com.pw.backendDiploma.Dao.Queries.AppointmentQueries;
import com.pw.backendDiploma.Entity.AppointmentEntity;
import com.pw.backendDiploma.Service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.List;

@Service
public class AppointmentServiceImpl implements AppointmentService {


    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private AppointmentDao repository;


    @Override
    public List<AppointmentEntity> findAppointmentEntityBySpecialisation(String specialisation) {
        List<AppointmentEntity> listActualAppointmentForSpecialisation =
                entityManager.createNamedQuery(AppointmentQueries.BY_SPECIALISATION, AppointmentEntity.class)
                        .setParameter("specialisation", specialisation)
                        .getResultList();
        if (listActualAppointmentForSpecialisation.size() > 0) {
            return listActualAppointmentForSpecialisation;
        } else
            return null;
    }

    @Override
    public List<AppointmentEntity> findAppointmentEntityBySpecialisationAndDate(String specialisation, Timestamp dateAppointment) {
        List<AppointmentEntity> listActualAppointmentForSpecialisationAndDate =
                entityManager.createNamedQuery(AppointmentQueries.BY_SPECIALISATION_AND_DATE, AppointmentEntity.class)
                        .setParameter("specialisation", specialisation).setParameter("dateAppointment", dateAppointment).getResultList();
        if (listActualAppointmentForSpecialisationAndDate.size() > 0) {
            return listActualAppointmentForSpecialisationAndDate;
        } else
            return null;
    }

    @Override
    public void updateStatusAppointment(int id) {
        AppointmentEntity appointmentEntity = findOne(id);
        appointmentEntity.setStatus("RESERVED");
        update(appointmentEntity);
    }

    @Override
    public void delete(AppointmentEntity appointmentEntity) {

    }

    @Override
    public List<AppointmentEntity> findAll() {
        return null;
    }

    @Override
    public AppointmentEntity findOne(int id) {
        return repository.findOne(id);
    }

    @Override
    public AppointmentEntity save(AppointmentEntity appointmentEntity) {
        return null;
    }

    @Override
    public AppointmentEntity update(AppointmentEntity appointmentEntity) {
        return repository.save(appointmentEntity);
    }
}
