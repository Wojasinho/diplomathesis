package com.pw.backendDiploma.Service.ServiceImpl;

import com.pw.backendDiploma.Service.HaversineService;
import org.springframework.stereotype.Service;

@Service
public class HaversineServiceImpl implements HaversineService {

    private static final int EARTH_RADIUS = 6371; // Approx Earth radius in KM

    public double haverSin(double val) {
        return Math.pow(Math.sin(val / 2), 2);
    }

    @Override
    public String parserForLocation(String location) {
        location = location.replace("[", "");
        location = location.replace("]", "");
        return location;
    }

    @Override
    public double calculateDistance(double startLat, double startLong, double endLat, double endLong) {
        double dLat = Math.toRadians((endLat - startLat));
        double dLong = Math.toRadians((endLong - startLong));

        startLat = Math.toRadians(startLat);
        endLat = Math.toRadians(endLat);

        double a = haverSin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haverSin(dLong);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        // return distance in [km] between points
        return EARTH_RADIUS * c; // <-- d
    }
}