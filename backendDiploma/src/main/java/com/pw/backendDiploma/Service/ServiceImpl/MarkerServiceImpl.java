package com.pw.backendDiploma.Service.ServiceImpl;

import com.pw.backendDiploma.Dao.MarkerRepository;
import com.pw.backendDiploma.Entity.MarkerEntity;
import com.pw.backendDiploma.Service.MarkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarkerServiceImpl implements MarkerService {

    @Autowired
    private MarkerRepository markerRepository;

    @Override
    public List<MarkerEntity> findAll() {
        return markerRepository.findAll();
    }
}