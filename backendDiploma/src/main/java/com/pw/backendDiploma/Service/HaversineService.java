package com.pw.backendDiploma.Service;

public interface HaversineService {

    double calculateDistance(double startLat, double startLong, double endLat, double endLong);

    double haverSin(double val);

    String parserForLocation(String location);
}
