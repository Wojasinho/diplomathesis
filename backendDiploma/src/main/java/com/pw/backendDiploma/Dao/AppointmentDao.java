package com.pw.backendDiploma.Dao;

import com.pw.backendDiploma.Entity.AppointmentEntity;
import com.pw.backendDiploma.Entity.MarkerEntity;
import com.pw.backendDiploma.Entity.SpecialisationEntity;
import org.springframework.data.repository.Repository;

import java.sql.Timestamp;
import java.util.List;

public interface AppointmentDao extends Repository<AppointmentEntity, Integer> {

    void delete(AppointmentEntity appointmentEntity);

    List<AppointmentEntity> findAll();

    AppointmentEntity findOne(int id);

    AppointmentEntity save(AppointmentEntity appointmentEntity);

}
