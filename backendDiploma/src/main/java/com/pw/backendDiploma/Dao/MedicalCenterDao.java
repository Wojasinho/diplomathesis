package com.pw.backendDiploma.Dao;

import com.pw.backendDiploma.Entity.MedicalCenterEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface MedicalCenterDao extends CrudRepository<MedicalCenterEntity, Integer> {

    List<MedicalCenterEntity> findAll();
}
