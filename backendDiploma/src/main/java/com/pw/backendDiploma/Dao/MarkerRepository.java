package com.pw.backendDiploma.Dao;

import com.pw.backendDiploma.Entity.MarkerEntity;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface MarkerRepository extends Repository<MarkerEntity, Integer> {

    List<MarkerEntity> findAll();


}