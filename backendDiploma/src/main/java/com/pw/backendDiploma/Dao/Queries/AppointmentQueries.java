package com.pw.backendDiploma.Dao.Queries;

public class AppointmentQueries {

        public static final String BY_SPECIALISATION = "AppointmentEntity.findAppointmentEntityBySpecialisation";

        public static final String BY_SPECIALISATION_AND_DATE = "AppointmentEntity.findAppointmentEntityBySpecialisationAndDate";

        public static final String UPDATE_STATUS_APPOINTMENT = "AppointmentEntity.updateStatusAppointment";
}
