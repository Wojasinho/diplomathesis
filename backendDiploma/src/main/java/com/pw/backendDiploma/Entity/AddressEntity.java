package com.pw.backendDiploma.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "ADDRESS")
public class AddressEntity {

    @Id
    @Column(name = "ID_ADDRESS", nullable = false)
    private long id;

    @Column(name = "STREET", length = 45, nullable = false)
    private String street;

    @Column(name = "NUM", length = 11, nullable = false)
    private String num;

    @Column(name = "POST_CODE", nullable = false)
    private String postCode;

    @Column(name = "CITY", nullable = false)
    private String city;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
