package com.pw.backendDiploma.Entity;

import com.pw.backendDiploma.Entity.Enum.Gender;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "PATIENT")
public class PatientEntity {

    @Id
    @Column(name = "ID_PATIENT", nullable = false)
    private long id;

    @Column(name = "NAME", length = 20)
    private String name;

    @Column(name = "LASTNAME", length = 20)
    private String lastname;

    @Enumerated(EnumType.STRING)
    @Column(name = "GENDER")
    public Gender gender;

    @Column(name = "EMAIL", length = 40)
    private String email;

    @Column(name = "PESEL", length = 11)
    private String pesel;

    @Column(name = "PHONE_NUMBER", length = 20)
    private String phoneNumber;

    @Column(name = "DATE_BECOME_PATIENT")
    private Date dateBecomePatient;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_ADDRESS_FK")
    private AddressEntity addressEntity;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getDateBecomePatient() {
        return dateBecomePatient;
    }

    public void setDateBecomePatient(Date dateBecomePatient) {
        this.dateBecomePatient = dateBecomePatient;
    }

    public AddressEntity getAddressEntity() {
        return addressEntity;
    }

    public void setAddressEntity(AddressEntity addressEntity) {
        this.addressEntity = addressEntity;
    }
}
