package com.pw.backendDiploma.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GRADE")
public class GradeEntity {

    @Id
    @Column(name = "ID_GRADE", nullable = false)
    private long id;

    @Column(name = "GRADE_SHORT", length = 25)
    private String gradeShort;

    @Column(name = "GRADE_LONG", length = 50, nullable = false)
    private String gradeLong;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGradeShort() {
        return gradeShort;
    }

    public void setGradeShort(String gradeShort) {
        this.gradeShort = gradeShort;
    }

    public String getGradeLong() {
        return gradeLong;
    }

    public void setGradeLong(String gradeLong) {
        this.gradeLong = gradeLong;
    }
}
