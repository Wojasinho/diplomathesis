package com.pw.backendDiploma.Entity;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;

@Entity
@Table(name = "OPINION")
public class OpinionEntity {

    @Id
    @Column(name = "ID_OPINION", nullable = false)
    private long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_APPOINTMENT_FK")
    private AppointmentEntity appointmentEntity;

    @Range(min = 0, max = 10)
    @Column(name = "OPINION_VALUE")
    private String opinionValue;

    @Column(name = "OPINION_DESCRIPTION")
    private String opinionDescription;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public AppointmentEntity getAppointmentEntity() {
        return appointmentEntity;
    }

    public void setAppointmentEntity(AppointmentEntity appointmentEntity) {
        this.appointmentEntity = appointmentEntity;
    }

    public String getOpinionValue() {
        return opinionValue;
    }

    public void setOpinionValue(String opinionValue) {
        this.opinionValue = opinionValue;
    }

    public String getOpinionDescription() {
        return opinionDescription;
    }

    public void setOpinionDescription(String opinionDescription) {
        this.opinionDescription = opinionDescription;
    }
}
