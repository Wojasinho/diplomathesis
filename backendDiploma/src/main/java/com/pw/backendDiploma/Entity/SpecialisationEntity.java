package com.pw.backendDiploma.Entity;

import javax.persistence.*;

@Entity
@Table(name = "SPECIALISATION")
public class SpecialisationEntity {

    @Id
    @Column(name = "ID_SPECIALISATION", nullable = false)
    private long id;

    @Column(name = "SPECIALISATION", length = 50, nullable = false)
    private String specialisation;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSpecialisation() {
        return specialisation;
    }

    public void setSpecialisation(String specialisation) {
        this.specialisation = specialisation;
    }
}