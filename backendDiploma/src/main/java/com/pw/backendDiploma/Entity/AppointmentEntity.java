package com.pw.backendDiploma.Entity;

import com.pw.backendDiploma.Dao.Queries.AppointmentQueries;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "APPOINTMENT")
@NamedQueries({
        @NamedQuery(name = AppointmentQueries.BY_SPECIALISATION,
                query = "SELECT c FROM AppointmentEntity c INNER JOIN c.doctorEntity d " +
                        "INNER JOIN d.listSpecialisations e WHERE e.specialisation = :specialisation AND c.statusAppointment = null "),

        @NamedQuery(name = AppointmentQueries.BY_SPECIALISATION_AND_DATE,
                query = "SELECT c FROM AppointmentEntity c INNER JOIN c.doctorEntity d " +
                        "INNER JOIN d.listSpecialisations e " +
                        "WHERE e.specialisation = :specialisation AND c.dateAppointment >= :dateAppointment AND c.statusAppointment = null "),

        @NamedQuery(name = AppointmentQueries.UPDATE_STATUS_APPOINTMENT,
                query = "UPDATE AppointmentEntity c SET c.statusAppointment = 'reserved' where c.id = 1")
})

public class AppointmentEntity {

    @Id
    @Column(name = "ID_APPOINTMENT", nullable = false)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_PATIENT_FK")
    private PatientEntity patientEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_DOCTOR_FK")
    private DoctorEntity doctorEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_MEDICAL_CENTER_FK")
    private MedicalCenterEntity medicalCenterEntity;

    @Column(name = "DATE_APPOINTMENT")
    private Timestamp dateAppointment;

    @Column(name = "STATUS_APPOINTMENT", length = 400)
    private String statusAppointment;

    @Column(name = "PRESCRIPTION", length = 400)
    private String prescription;

    @Column(name = "DESCRIPTION_APPOINTMENT", length = 1000)
    private String descriptionAppointment;

    @Column(name = "SICK_LEAVE")
    private Date sickLeave;

    @Column(name = "TOTAL_COST_OF_VISIT")
    private double totalCostOfVisit;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PatientEntity getPatientEntity() {
        return patientEntity;
    }

    public void setPatientEntity(PatientEntity patientEntity) {
        this.patientEntity = patientEntity;
    }

    public DoctorEntity getDoctorEntity() {
        return doctorEntity;
    }

    public void setDoctorEntity(DoctorEntity doctorEntity) {
        this.doctorEntity = doctorEntity;
    }

    public MedicalCenterEntity getMedicalCenterEntity() {
        return medicalCenterEntity;
    }

    public void setMedicalCenterEntity(MedicalCenterEntity medicalCenterEntity) {
        this.medicalCenterEntity = medicalCenterEntity;
    }

    public Timestamp getDateAppointment() {
        return dateAppointment;
    }

    public void setDateAppointment(Timestamp dateAppointment) {
        this.dateAppointment = dateAppointment;
    }

    public String getStatus() {
        return statusAppointment;
    }

    public void setStatus(String statusAppointment) {
        this.statusAppointment = statusAppointment;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getDescriptionAppointment() {
        return descriptionAppointment;
    }

    public void setDescriptionAppointment(String descriptionAppointment) {
        this.descriptionAppointment = descriptionAppointment;
    }

    public Date getSickLeave() {
        return sickLeave;
    }

    public void setSickLeave(Date sickLeave) {
        this.sickLeave = sickLeave;
    }

    public double getTotalCostOfVisit() {
        return totalCostOfVisit;
    }

    public void setTotalCostOfVisit(double totalCostOfVisit) {
        this.totalCostOfVisit = totalCostOfVisit;
    }
}
