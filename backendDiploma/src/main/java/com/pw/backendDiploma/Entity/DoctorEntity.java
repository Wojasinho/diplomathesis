package com.pw.backendDiploma.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "DOCTOR")
public class DoctorEntity {

    @Id
    @Column(name = "ID_DOCTOR", nullable = false)
    private long id;

    @Column(name = "NAME", length = 20)
    private String name;

    @Column(name = "LASTNAME", length = 20)
    private String lastname;

    @Column(name = "EMAIL", length = 40)
    private String email;

    @Column(name = "PESEL", length = 11)
    private String pesel;

    @Column(name = "PHONE_NUMBER", length = 12)
    private String phoneNumber;

    @Column(name = "PWZ_NUMBER", length = 7)
    private String pwzNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_GRADE_FK")
    private GradeEntity gradeEntity;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "DOCTOR_SPECIALISATION",
            joinColumns = {@JoinColumn(name = "ID_DOCTOR_FK")},
            inverseJoinColumns = {@JoinColumn(name = "ID_SPECIALISATION_FK")})
    private List<SpecialisationEntity> listSpecialisations = new ArrayList<SpecialisationEntity>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPwzNumber() {
        return pwzNumber;
    }

    public void setPwzNumber(String pwzNumber) {
        this.pwzNumber = pwzNumber;
    }

    public GradeEntity getGradeEntity() {
        return gradeEntity;
    }

    public void setGradeEntity(GradeEntity gradeEntity) {
        this.gradeEntity = gradeEntity;
    }

    public List<SpecialisationEntity> getListSpecialisations() {
        return listSpecialisations;
    }

    public void setListSpecialisations(List<SpecialisationEntity> listSpecialisations) {
        this.listSpecialisations = listSpecialisations;
    }
}
