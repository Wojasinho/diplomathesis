package com.pw.backendDiploma.Entity;

import javax.persistence.*;

@Entity
@Table(name = "MedicalCenter")
public class MedicalCenterEntity {

    @Id
    @Column(name = "ID_MEDICAL_CENTER")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "LATITUDE")
    private float latitude;

    @Column(name = "LONGITUDE")
    private float longitude;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "DRAGGABLE")
    private Boolean draggable;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_ADDRESS_FK")
    private AddressEntity addressEntity;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getDraggable() {
        return draggable;
    }

    public void setDraggable(Boolean draggable) {
        this.draggable = draggable;
    }

    public AddressEntity getAddressEntity() {
        return addressEntity;
    }

    public void setAddressEntity(AddressEntity addressEntity) {
        this.addressEntity = addressEntity;
    }
}

